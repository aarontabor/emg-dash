# EMG Dash

A web-based EMG visualizer built using the Dash/plotly.py frameworks

![Screenshot](./images/emg-dash.jpg)


# Installation

This project uses `pipenv` for dependency managenment. However, a private, in-house library is also used for streaming EMG data. Therefore installation is completed in two steps:

## Step 1: Install public dependencies

Run the following commands from the project root to install project dependencies and use the newly created virtual python environment:

```
pipenv install
pipenv shell
```


## Step 2: Install the private `biomedical-hardware-readers` package

The `biomedical-hardware-readers` library is a private, IBME-developed python package - installation is not currently automated through `pipenv`. Install the package by following these steps:

1) **Clone the [repository](https://github.com/stallam-unb/biomedical-hardware-readers)**. If you are unable to access the link, contact [Shriram](mailto:Shriram.TallamPuranamRaghu@unb.ca) for access.

1) **Install the package**. Run the following command from the `EMG Dash` project root folder:

```
pipenv run pip install <path/to/biomedical-hardware-readers/repo>
```

1) **Delete the repository (optional)**. At this point, the `biomedical-hardware-sensors` repository is no longer needed, and can be safely removed (i.e., `pipenv` has copied the relevant files into it's local cache).


# Running the application

For development purposes, we can simply run the app using Dash's built-in server. Note that I've set the `debug=True` flag, which enables hot-reloading. Run the following command to launch the server:

```
python app.py
```

By default, Dash applications are hosted on port `8050`. Once your app is running, click [here](http://127.0.0.1:8050/) to view it in the browser.


# Troubleshooting

## Callback failed: the server did not respond

This is likely caused by an excessively high framerate (i.e., the app is requesting to be refreshed faster than the server can keep up). Reduce the `max_framerate` variable in `app.py`.

Note: I have currently implemented the real-time streaming naively (i.e., recompute and retransmit the dataset *every* frame). We can likely achieve *large* performance improvements with a little more work. However, current approach is simple and probably good enough for now.

## Mysterious callback key errors

This is likely caused by a stale version of the client app cached on your browser (i.e., looking for a callback that longer exists). Exit the tab / browser and try again - simply refreshing the page will likely maintain the stale cache.


# Caveats

## Plotly's Interactive Graph Controls

In general, plotly allows users to manipulate visualized data (e.g., zoom, pan, filter). However, these settings don't "stick" in this example becase the entire graph is redrawn from scratch each frame. If we care about this, we will need to preserve the graph between frames (likely by using a [`clientside-callback`](https://dash.plotly.com/clientside-callbacks))
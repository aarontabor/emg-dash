from dash import Dash, html, dcc, Output, Input
import plotly.express as px

from AsyncReader import AsyncReader

max_framerate = 15

app_text = '''
# EMG Visualizer
This is a proof-of-concept application writting in [Dash](https://dash.plotly.com/). 
The app streams data using [Shri's `biomedical_hardware_readers`](https://github.com/stallam-unb/biomedical-hardware-readers). 
Currently, I have used his `VirtualReader` to streamline development.
'''


def main():
    with AsyncReader() as reader:
        app = Dash(__name__)

        app.layout = html.Div(children=[
            dcc.Markdown(app_text),
            dcc.Graph(id='emg-graph'),
            dcc.Interval(
                id='interval-component',
                interval=(1000/max_framerate) if max_framerate else 1,
                n_intervals=0,  # increments monotomically each interval - used to trigger the callback below
            ),
        ])

        @app.callback(Output('emg-graph', 'figure'), Input('interval-component', 'n_intervals'))
        def draw_graph(_):
            df = reader.data
            df_long = df.melt(
                id_vars=['elapsed_sec'],
                var_name='sensor_id',
                value_name='reading',
            )
            return px.scatter(
                data_frame=df_long,
                x='elapsed_sec',
                y='reading',
                color='sensor_id',
                facet_row='sensor_id',
                height=2000,
            )

        app.run_server(debug=True)


if __name__ == '__main__':
    main()
